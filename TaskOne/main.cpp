#include <iostream>
#include <cstring>
#include "apue.h"
#include <fcntl.h>

#define MAXLINE 4096

static void err_doit(int errnoflag, int error, const char *fmt, va_list ap) {
    char buf[MAXLINE];

    vsnprintf(buf, MAXLINE - 1, fmt, ap);
    if (errnoflag)
        snprintf(buf + strlen(buf), MAXLINE - strlen(buf) - 1, ": %s",
                 strerror(error));
    strcat(buf, "\n");
    fflush(stdout); /* in case stdout and stderr are the same */
    fputs(buf, stderr);
    fflush(nullptr); /* flushes all stdio output streams */
}

void err_sys(const char *fmt, ...) {
    va_list ap;

    va_start(ap, fmt);
    err_doit(1, errno, fmt, ap);
    va_end(ap);
    exit(1);
}

bool check(int argc, char **argv) {
    const std::string hole = "-h";
    const std::string data = "-d";
    try {
        for (int i = 1; i < argc; i++) {
            if (argv[i] == data) {
                i++;
                if (i >= argc) {
                    return false;
                }
                continue;
            }
            if (argv[i] == hole) {
                i++;
                if (i >= argc) {
                    return false;
                }
                if (std::stoi(argv[i]) < 0) {
                    return false;
                }
                continue;
            }
            return false;
        }
    } catch (std::invalid_argument &e) {
        printf("Invalid argument");
        return false;
    } catch (std::out_of_range &e) {
        printf("Out of range");
        return false;
    }
    return true;
}


int main(int argc, char **argv) {
    const std::string hole = "-h";
    const std::string data = "-d";
    if (!check(argc, argv)) {
        return -1;
    }
    int fd;
    if ((fd = creat("file.hole", FILE_MODE)) < 0)
        err_sys("ошибка вызова creat");
    for (int i = 1; i < argc; i++) {
        if (argv[i] == data) {
            std::string s = argv[i + 1];
            if (write(fd, argv[i + 1], s.size()) != s.size()) {
                err_sys("ошибка записи");
            }
            i++;
        }
        if (argv[i] == hole) {
            if (lseek(fd, std::stoi(argv[i + 1]), SEEK_SET) == -1) {
                err_sys("ошибка вызова lseek");
            }
            i++;
        }
    }
    return 0;
}
