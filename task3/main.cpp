#include <iostream>
#include <unistd.h>
#include <sys/stat.h>
#include <dirent.h>
#include <stdexcept>

bool isStatEquals(struct stat *stat1, struct stat *stat2){
    return stat1->st_dev == stat2->st_dev && stat1->st_ino == stat2->st_ino;
}

int main() {
    struct stat root_stat, cwd_stat, cur_stat, temp_stat;
    const std::string UPDIR = "..";
    const std::string CURDIR = ".";
    struct dirent *pDirent;
    DIR *dir;
    int count = 0;
    int lvl = -1;

    if(stat("/",&root_stat) < 0){
        throw std::runtime_error("Can`t get root stat");
    }
    if(stat(CURDIR.c_str(), &cwd_stat) < 0){
       throw std::runtime_error("Can`t get current stat");
    }
    do {
        lvl++;
        if(lvl % 2 != 0){
            if(stat(CURDIR.c_str(), &temp_stat) < 0){
                throw std::runtime_error("Can`t get current dir stat");
            }
            if(chdir(UPDIR.c_str()) < 0){
                throw std::runtime_error("Can`t open dir");
            }
            continue;
        }
        if((dir = opendir(CURDIR.c_str())) == nullptr){
            throw std::runtime_error("Can`t open dir");
        }
        while((pDirent = readdir(dir)) != nullptr){
            if(CURDIR == pDirent->d_name || UPDIR == pDirent->d_name ){
                continue;
            }
            if(lstat(pDirent->d_name, &cur_stat) < 0){
                throw std::runtime_error("");
            }
            if(S_ISLNK(cur_stat.st_mode)){
                if(stat(pDirent->d_name, &cur_stat) == 0 && isStatEquals(&cwd_stat, &cur_stat)){
                    count++;
                }
            }
        }
        closedir(dir);
        if(stat(CURDIR.c_str(), &temp_stat) < 0){
            throw std::runtime_error("Can`t get current dir stat");
        }
        if(chdir(UPDIR.c_str()) < 0){
            throw std::runtime_error("Can`t open dir");
        }

    }while(!isStatEquals(&temp_stat, &root_stat));
    
    printf("Кол-во символьных ссылок: %d\n",count);
    return 0;
}
