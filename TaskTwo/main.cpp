#include <stdexcept>
#include <unistd.h>


bool checkArguments(int argc, char** argv) {
    if (argc < 2) {
        printf("Invalid buffer size\n");
        return false;
    }
    try {
        int bufferSize = std::stoi(argv[1]);
        if (bufferSize > 0) {
            return true;
        }
    } catch (std::invalid_argument&) {
        printf("Incorrect argument\n");
    } catch (std::out_of_range&) {
        printf("Out of range\n");
    }
    return false;
}

void copyFile(int bufferSize) {
    void* buf = malloc(bufferSize);
    bool flag = true;
    int bytesRead;

    while (flag) {
        bytesRead = read(STDIN_FILENO, buf, bufferSize);
        flag = bytesRead > 0;
        if (flag) {
            write(STDOUT_FILENO, buf, bytesRead);
        }
    }

    free(buf);
}

int main(int argc, char* argv[]){
    if(!checkArguments(argc, argv)){
        return -1;
    }
    copyFile(std::stoi(argv[1]));
    return 0;
}
