#include <iostream>
#include <pthread.h>
#include <fstream>

using namespace std;

fstream out("task4.txt", std::ios::out);

const int client_count = 2;
const string QUIT = "quit";

std::string buf;
int strings_num = 0;
int clients_work = 0;


pthread_rwlock_t buf_lock;
pthread_rwlock_t clients_lock;
pthread_rwlock_t string_lock;
pthread_rwlock_t out_lock;

void *server(void *argp) {
    bool flag;
    bool quit_flag;
    while (1) {
        pthread_rwlock_rdlock(&clients_lock);
        flag = clients_work == 0;
        pthread_rwlock_unlock(&clients_lock);
        if (flag) {
            pthread_rwlock_wrlock(&buf_lock);
            cin >> buf;
            pthread_rwlock_unlock(&buf_lock);

            pthread_rwlock_wrlock(&string_lock);
            strings_num++;
            pthread_rwlock_unlock(&string_lock);

            pthread_rwlock_wrlock(&clients_lock);
            clients_work = client_count;
            pthread_rwlock_unlock(&clients_lock);

            pthread_rwlock_rdlock(&buf_lock);
            quit_flag = buf == QUIT;
            pthread_rwlock_unlock(&buf_lock);
            if (quit_flag) {
                break;
            }
        }
    }
    pthread_exit(0);
}

void *client(void *argp) {
    bool flag;
    bool quit_flag;
    int local_strings = 0;
    while (1) {
        pthread_rwlock_rdlock(&string_lock);
        flag = local_strings != strings_num;
        pthread_rwlock_unlock(&string_lock);

        if (flag) {
            pthread_rwlock_rdlock(&buf_lock);
            quit_flag = buf == QUIT;
            pthread_rwlock_unlock(&buf_lock);

            if (quit_flag) {
                break;
            }
            pthread_rwlock_wrlock(&out_lock);
            pthread_rwlock_rdlock(&buf_lock);
            out << &pthread_self << ": " << buf << std::endl;
            pthread_rwlock_unlock(&buf_lock);
            pthread_rwlock_unlock(&out_lock);

            pthread_rwlock_rdlock(&string_lock);
            local_strings = strings_num;
            pthread_rwlock_unlock(&string_lock);

            pthread_rwlock_wrlock(&clients_lock);
            clients_work--;
            pthread_rwlock_unlock(&clients_lock);
        }
    }
    pthread_exit(0);
}


int main() {
    pthread_t server_thread, clients_thread[client_count];

    pthread_rwlock_init(&buf_lock, 0);
    pthread_rwlock_init(&clients_lock, 0);
    pthread_rwlock_init(&string_lock, 0);
    pthread_rwlock_init(&out_lock, 0);


    pthread_create(&server_thread, 0, server, 0);
    for (int i = 0; i < client_count; i++) {
        pthread_create(&(clients_thread[i]), 0, client, 0);
    }

    pthread_join(server_thread, 0);
    for (int i = 0; i < client_count; i++) {
        pthread_join(clients_thread[i], 0);
    }
    pthread_rwlock_destroy(&buf_lock);
    pthread_rwlock_destroy(&clients_lock);
    pthread_rwlock_destroy(&string_lock);
    pthread_rwlock_destroy(&out_lock);

    return 0;
}
